//
//  ViewController.swift
//  Polyglot
//
//  Created by Danni Brito on 6/6/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var words = [String]()

    
    //MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleAttributes = [
            NSAttributedString.Key.font: UIFont(name: "AmericanTypewriter", size: 22)
        ]
        
        navigationController?.navigationBar.titleTextAttributes = titleAttributes as [NSAttributedString.Key : Any]
        title = "POLYGLOT"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewWord))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(startTest))
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "End Test", style: .plain, target: nil, action: nil)
        
        if let defaults = UserDefaults(suiteName: "group.com.danni.polyglot") {
            if let savedWords = defaults.object(forKey: "Words") as? [String] {
                words = savedWords
            } else {
                saveInitialValues(to: defaults)
            }
        }
    }
    
    //MARK: - Table View Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
        
        let word = words[indexPath.row]
        let split = word.components(separatedBy: "::")
        
        cell.textLabel?.text = split[0]
        cell.detailTextLabel?.text = ""
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) {
            if cell.detailTextLabel?.text == "" {
                let word = words[indexPath.row]
                let split = word.components(separatedBy: "::")
                
                cell.detailTextLabel?.text = split[1]
            } else {
                cell.detailTextLabel?.text = ""
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        words.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        saveWords()
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [unowned self] (action, view, handler) in
            print("delete pressed")
            self.words.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.saveWords()
        }
        let editAction = UIContextualAction(style: .normal, title: "Edit") { [unowned self] (action, view, handler) in
            let ac = UIAlertController(title: "Edit", message: nil, preferredStyle: .alert)
            ac.addTextField { (textField) in
                textField.placeholder = "English"
                textField.text = self.words[indexPath.row].components(separatedBy: "::")[0]
            }
            ac.addTextField { (textField) in
                textField.placeholder = "French"
                textField.text = self.words[indexPath.row].components(separatedBy: "::")[1]
            }
            
            let submitAction = UIAlertAction(title: "Edit", style: .default) { [unowned self, ac] (action) in
                let firstWord = ac.textFields?[0].text ?? ""
                let secondWord = ac.textFields?[1].text ?? ""
                
                self.words[indexPath.row] = "\(firstWord)::\(secondWord)"
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            
            ac.addAction(submitAction)
            ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(ac, animated: true, completion: nil)
            
        }
        
        editAction.backgroundColor = UIColor.systemBlue
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        return configuration
    }
    
    
    
    // MARK: - Other Methods
    func saveInitialValues(to defaults: UserDefaults) {
        words.append("bear::l'ours")
        words.append("camel::le chameau")
        words.append("cow::la vache")
        words.append("fox::le renard")
        words.append("goat::la chèvre")
        words.append("monkey::le singe")
        words.append("pig::le cochon")
        words.append("rabbit::le lapin")
        words.append("sheep::le mouton")
        
        defaults.set(words, forKey: "Words")
    }

    func saveWords() {
        if let defaults = UserDefaults(suiteName: "group.com.danni.polyglot") {
            defaults.set(words, forKey: "Words")
        }
    }
    
    @objc func addNewWord() {
        // create our alert controller
        let ac = UIAlertController(title: "Add new word", message: nil, preferredStyle: .alert)
        
        // add two text fields, one for English and one for french
        ac.addTextField { (textField) in
            textField.placeholder = "English"
        }
        ac.addTextField { (textField) in
            textField.placeholder = "French"
        }
        
        // create an "Add word" button that submits the user's input
        let submitAction = UIAlertAction(title: "Add Word", style: .default) { [unowned self, ac] (action) in
            // pull out the english and french word, or an empty string if there was a problem
            let firstWord = ac.textFields?[0].text ?? ""
            let secondWord = ac.textFields?[1].text ?? ""
            
            // submit the english and french words to the insertFlashcard() method
            self.insertFlashcard(first: firstWord, second: secondWord)
        }
        
        // add the submit action, plus a cancel button
        ac.addAction(submitAction)
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        // present the alert controller to the user
        present(ac, animated: true, completion: nil)
    }
    
    func insertFlashcard(first: String, second: String) {
        guard first.count > 0 && second.count > 0 else { return }
        
        let newIndexPath = IndexPath(row: words.count, section: 0)
        
        words.append("\(first)::\(second)")
        tableView.insertRows(at: [newIndexPath], with: .automatic)
        
        saveWords()
    }
    
    @objc func startTest() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "Test") as? TestViewController else { return }
        vc.words = words
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

